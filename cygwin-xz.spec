%{?cygwin_package_header}

Name:      cygwin-xz
Version:   5.2.3
Release:   1%{?dist}
Summary:   LZMA library for Cygwin toolchains

Group:     Development/Libraries
License:   Public Domain
URL:       http://tukaani.org/xz/
BuildArch: noarch

Source0:   http://tukaani.org/xz/xz-%{version}.tar.xz

BuildRequires: autoconf automake gettext-devel cygwin-libtool-base

BuildRequires: cygwin32-filesystem
BuildRequires: cygwin32-binutils
BuildRequires: cygwin32-gcc
BuildRequires: cygwin32
BuildRequires: cygwin64-filesystem
BuildRequires: cygwin64-binutils
BuildRequires: cygwin64-gcc
BuildRequires: cygwin64


%description
LZMA library for Cygwin toolchains

%package -n cygwin32-xz-libs
Summary:   LZMA library for Cygwin32 toolchain
Group:     Development/Libraries

%description -n cygwin32-xz-libs
LZMA library for Cygwin i686 toolchain

%package -n cygwin64-xz-libs
Summary:   LZMA library for Cygwin64 toolchain
Group:     Development/Libraries

%description -n cygwin64-xz-libs
LZMA library for Cygwin x86_64 toolchain

%{?cygwin_debug_package}


%prep
%setup -q -n xz-%{version}
%cygwin_autoreconf


%build
%cygwin_configure --enable-shared --disable-static \
    --disable-lzmadec --disable-lzmainfo --disable-lzma-links \
    --disable-scripts --disable-xz --disable-xzdec
%cygwin_make %{?_smp_mflags}


%install
%cygwin_make install DESTDIR=$RPM_BUILD_ROOT

# We intentionally don't ship *.la files
find $RPM_BUILD_ROOT -name '*.la' -delete

# Documentation already provided by Fedora native package
rm -fr $RPM_BUILD_ROOT%{cygwin32_docdir}/
rm -fr $RPM_BUILD_ROOT%{cygwin32_mandir}/
rm -fr $RPM_BUILD_ROOT%{cygwin64_docdir}/
rm -fr $RPM_BUILD_ROOT%{cygwin64_mandir}/

# Remove unnecessary Cygwin native binaries
rm -f $RPM_BUILD_ROOT%{cygwin32_bindir}/*.exe
rm -f $RPM_BUILD_ROOT%{cygwin64_bindir}/*.exe


%files -n cygwin32-xz-libs
%doc ChangeLog COPYING NEWS README
%{cygwin32_bindir}/cyglzma-5.dll
%{cygwin32_includedir}/lzma.h
%{cygwin32_includedir}/lzma/
%{cygwin32_libdir}/liblzma.dll.a
%{cygwin32_libdir}/pkgconfig/liblzma.pc

%files -n cygwin64-xz-libs
%doc ChangeLog COPYING NEWS README
%{cygwin64_bindir}/cyglzma-5.dll
%{cygwin64_includedir}/lzma.h
%{cygwin64_includedir}/lzma/
%{cygwin64_libdir}/liblzma.dll.a
%{cygwin64_libdir}/pkgconfig/liblzma.pc


%changelog
* Tue Dec 05 2017 Yaakov Selkowitz <yselkowi@redhat.com> - 5.2.3-1
- new version

* Wed Mar 30 2016 Yaakov Selkowitz <yselkowi@redhat.com> - 5.2.2-1
- new version

* Tue Mar 10 2015 Yaakov Selkowitz <yselkowi@redhat.com> - 5.0.8-1
- new version

* Tue Jun 17 2014 Yaakov Selkowitz <cygwin-ports-general@lists.sourceforge.net> - 5.0.5-1
- Initial RPM release.
